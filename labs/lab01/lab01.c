/**
 * @brief LAB #01 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */
#include "pico/stdlib.h"

int main() {

    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    void flash(int pin_no, int sleep_delay){
        // Toggle the LED on and then sleep for delay period
        gpio_put(pin_no, 1);
        sleep_ms(sleep_delay);

        // Toggle the LED off and then sleep for delay period
        gpio_put(pin_no, 0);
        sleep_ms(sleep_delay);
    }

    // Do forever...
    while (true) {
      flash(LED_PIN, LED_DELAY);
    }

    // Should never get here due to infinite while-loop.
    return 0;

}
